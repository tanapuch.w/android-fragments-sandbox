package com.example.fragmentsexample;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ExampleFragment extends Fragment {
    private static final String ARG_TEXT = "argText";
    private static final String ARG_NUMBER = "argNumber";

    private String text;
    private int number;

    public static ExampleFragment exampleFragment(String text, int number) {
        ExampleFragment fragment = new ExampleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        args.putInt(ARG_NUMBER, number);
        fragment.setArguments(args);
        return fragment;
    }

    public interface FragmentChangeListener
    {
        public void replaceFragment(Fragment fragment);
    }
/**
    private FragmentChangeListener listener;

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentChangeListener) {
            listener = (FragmentChangeListener) context;
        } else {
            throw new ClassCastException(context + "must implement " + FragmentChangeListener.class.getSimpleName());
        }
        super.onAttach(context);
    }
**/
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.example_fragment, container, false);
        TextView textView = v.findViewById(R.id.text_view_fragment);
        Button button = v.findViewById(R.id.button_example_fragment);

        if (getArguments() != null){
            text = getArguments().getString(ARG_TEXT);
            number = getArguments().getInt(ARG_NUMBER);
        }

        textView.setText(text + number);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOtherFragment();
            }
        });

        return v;
    }

    public void showOtherFragment()
    {
        Fragment fr = new FragmentA().fragmentA("NOW IS BLUE", 123);
        FragmentChangeListener fc = (FragmentChangeListener)getActivity();
        fc.replaceFragment(fr);
    }
}
