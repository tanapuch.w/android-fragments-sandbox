package com.example.fragmentsexample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentA extends Fragment {
    private static final String ARG_TEXT = "argText";
    private static final String ARG_NUMBER = "argNumber";

    private String text;
    private int number;

    public static FragmentA fragmentA(String text, int number) {
        FragmentA fragment = new FragmentA();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        args.putInt(ARG_NUMBER, number);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_a, container, false);
        TextView textView = v.findViewById(R.id.text_view_fragment_a);
        Button button = v.findViewById(R.id.button_fragment_a);

        if (getArguments() != null){
            text = getArguments().getString(ARG_TEXT);
            number = getArguments().getInt(ARG_NUMBER);
        }

        textView.setText(text + number);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Fragment fr = new ExampleFragment().exampleFragment("NOW IS GREEN ", 123);
                FragmentChangeListener fc = (FragmentChangeListener)getActivity();
                fc.replaceFragment(fr);
            }
        });

        return v;
    }

    public interface FragmentChangeListener
    {
        public void replaceFragment(Fragment fragment);
    }

    public void showOtherFragment()
    {
        Fragment fr = new ExampleFragment().exampleFragment("NOW IS GREEN ", 123);
        FragmentChangeListener fc = (FragmentChangeListener)getActivity();
        fc.replaceFragment(fr);
    }
}
