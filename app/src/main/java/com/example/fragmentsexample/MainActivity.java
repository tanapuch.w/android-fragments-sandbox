package com.example.fragmentsexample;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements ExampleFragment.FragmentChangeListener, FragmentA.FragmentChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ExampleFragment fragment = ExampleFragment.exampleFragment("example text ", 123);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit(); //Put the fragment into the FrameLayout called container
    }


    @Override
    public void replaceFragment(Fragment fragment) {
        //FragmentA fragmentA = FragmentA.fragmentA("now fragment A text ", 123);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }
}
